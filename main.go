package main

import "fmt"

func main() {
	// Beispiel 1: Definition einer Variable und eines Pointers darauf.
	//             Eine Veränderung der Variable ändert auch den Pointer und umgekehrt.

	// Die Variablen erzeugen und einmal ausgeben.
	v1 := 42
	p_v1 := &v1
	fmt.Println(v1, p_v1, *p_v1) // Gibt den Wert von v1 sowie die Adresse und den Wert von p_v1 aus.

	v1 = 23
	fmt.Println(v1, p_v1, *p_v1) // Es haben sich sowohl v1 als auch *p_v1 verändert.

	*p_v1 = 77
	fmt.Println(v1, p_v1, *p_v1) // Es haben sich sowohl v1 als auch *p_v1 verändert.

	// Beispiel 2: Eine Funktion nutzen, die einen Pointer erwartet und ihn verändert.
	changePtr(p_v1)
	fmt.Println(v1, p_v1, *p_v1) // Es haben sich sowohl v1 als auch *p_v1 verändert.

	// Beispiel 3: Einen Pointer mit new erzeugen.
	p2 := new(int)
	fmt.Println(p2, *p2) // Adresse und Wert von p2 ausgeben.
	changePtr(p2)
	fmt.Println(p2, *p2) // Adresse und Wert von p2 ausgeben.

	// Beispiel 4: Das Struct unten instanziieren und beide Methoden ausprobieren.
	s1 := MyStruct{10}
	fmt.Println(s1)
	s1.change()
	fmt.Println(s1)
	s1.noChange()
	fmt.Println(s1)

}

// Beispiel-Funktion: Erwartet einen int-Pointer und ändert den Wert dahinter.
func changePtr(i_ptr *int) {
	*i_ptr += 10
}

// Beispiel-Struct mit jeweils einer Methode, die es verändert und einer, die es nicht verändert.
type MyStruct struct {
	i1 int
}

func (m *MyStruct) change() {
	m.i1 += 100
}
func (m MyStruct) noChange() {
	m.i1 += 100 // Hat keinen Effekt, weil m nicht als Pointer vorliegt.
}
